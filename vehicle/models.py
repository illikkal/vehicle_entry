from django.db import models
from datetime import datetime

class Vehicle(models.Model):
    vehicle_name = models.CharField(max_length=200,blank=True,null=True)
    reg_number = models.CharField(max_length = 100)
    register_validity = models.DateField()

    def __str__(self):
        return "{},{}".format(self.vehicle_name,self.reg_number)


class Expense(models.Model):
    expense_type = models.CharField(max_length=200,help_text='eg:fuel expenses')

    def __str__(self):
        return self.expense_type

class ExpenseEntry(models.Model):
    expense_type = models.ForeignKey(Expense,on_delete=models.CASCADE,null=True,blank=True)
    vehicle =models.ForeignKey(Vehicle,on_delete=models.CASCADE)
    date = models.DateField(default=datetime.now())
    amount = models.FloatField()
    details = models.CharField(max_length=200,help_text='details about expense')

    def __str__(self):
        return "{} ${}".format(self.expense_type,self.amount)



