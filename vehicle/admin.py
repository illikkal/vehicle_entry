from django.contrib import admin
from django.contrib.auth.models import User, Group

from vehicle.models import Vehicle,Expense,ExpenseEntry

# Register your models h
admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(Vehicle)
admin.site.register(ExpenseEntry)
admin.site.register(Expense)